<?php theme_include('partial/header'); ?>

<section class="container" id="content">
	<div class="row">
		<div class="col-md-7">
			<article>
				<header>
					<h2>Lorem ipsum dolor sit amet enim. Etiam ullamcorper.</h2>
					<div class="meta clearfix">
						<p class="pull-left">
							Napisał: Piotr Filipek, 15 maja 2015
						</p>

						<div class="share-area pull-right">
						</div>
					</div>
				</header>

				<p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor.</p>
				<p>Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat.</p>
				
				<div class="keep-reading">
					<a href="#">Kontynuuj czytanie &raquo;</a>
				</div>
			</article>

			<article>
				<header>
					<h2>Lorem ipsum dolor sit amet enim. Etiam ullamcorper.</h2>
					<div class="meta">
						<p>Napisał: Piotr Filipek, 15 maja 2015</p>
					</div>
				</header>

				<p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor.</p>
				<p>Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat.</p>
				
				<h3>Podsumowanie...</h3>
				<p>Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, faucibus quis, porttitor eros cursus lectus, pellentesque eget, bibendum a, gravida ullamcorper quam. Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam sem. In hendrerit nulla quam nunc, accumsan congue. Lorem ipsum primis in nibh vel risus. Sed vel lectus. Ut sagittis, ipsum dolor quam.</p>

				<div class="tags">
					<ul>
						<li><a href="#">#css3</a></li>
						<li><a href="#">#html5</a></li>
						<li><a href="#">#javascript</a></li>
						<li><a href="#">#theme</a></li>
					</ul>
				</div>
			</article>
		</div>

		<div class="col-md-5">
			<div class="box">
				<h3>Ostatnio dodane</h3>
				<ul>
					<li><a href="#">Lorem ipsum dolor sit amet enim.</a></li>
					<li><a href="#">Suspendisse a pellentesque dui, non felis.</a></li>
				</ul>
			</div>

			<div class="box">
				<h3>Polecane strony</h3>
				<ul>
					<li><a href="#">Anchor - blogging system</a></li>
					<li><a href="#">Anchor Showcase - great sites made using Anchor CMS.</a></li>
					<li><a href="http://twitter.com/danoxide">@danoxide (Peter Filipek)</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<?php theme_include('partial/footer'); ?>