<?php theme_include('partial/header'); ?>
<section class="container" id="content">
	<div class="row">
		<div class="col-md-7">
			<article>
				<header>
					<h2><?php echo article_title(); ?></h2>
					<div class="meta clearfix">
						<p class="pull-left">
							Posted on 
							<time datetime="<?php echo article_date(); ?>">
								<?php echo article_date(); ?>
							</time>
							 by 
							<?php echo article_author('real_name'); ?>
							<?php if(user_authed()): ?>
								 / 
								<a href="<?php echo base_url('admin/posts/edit/' . article_id()); ?>">
									Edit
								</a>
							<?php endif; ?>.
						</p>
						<?php theme_include('partial/article_share'); ?>	
					</div>
				</header>
				<?php echo article_markdown(); ?>
			</article>

			<nav>
				<ul class="pager">
					<?php if(anaconda_is_prev()): ?>
						<li>
							<a href="<?php echo anaconda_prev('slug'); ?>" class="pull-left">
								<span class="glyphicon glyphicon-chevron-left"></span>
								 Previous article
							</a>
						</li>
					<?php endif; ?>
					<?php if(anaconda_is_next()): ?>
						<li>
							<a href="<?php echo anaconda_next('slug'); ?>" class="pull-right">
								Next article 
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</nav>

			<?php 
				if(comments_open()):
					theme_include('partial/comments_block');
				endif; 
			?>
		</div>
		<?php theme_include('partial/aside'); ?>
	</div>
</section>
<?php theme_include('partial/footer'); ?>