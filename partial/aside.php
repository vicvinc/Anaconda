<div class="col-md-5" id="aside">
	<div class="box">
		<div role="tabpanel">
			<ul class="nav nav-tabs nav-justified" role="tablist" id="recently">
				<li role="presentation" class="active">
					<a href="#posts_tab" aria-controls="posts_tab" role="tab" data-toggle="tab">
						Blog
					</a>
				</li>
				<li role="presentation">
					<a href="#comments_tab" aria-controls="comments_tab" role="tab" data-toggle="tab">
						Comments
					</a>
				</li>
			</ul>

			<div class="tab-content">

				<div role="tabpanel" class="tab-pane active" id="posts_tab">
					<ul>
					<?php foreach(anaconda_posts() as $post): ?>
						<li>
							<a href="<?php 
											echo article_page().$post->slug;
											//echo article_url(); 
										?>">
								<?php echo (strlen($post->title)>30?substr($post->title, 0, 30).'...':$post->title) ?>
							</a>
						</li>
					<?php endforeach; ?>
					</ul>
				</div>

				<div role="tabpanel" class="tab-pane" id="comments_tab">
				<!-- latter change this tab to categories -->
					<ul>
					<?php foreach(anaconda_comments() as $comment): ?>
						<li>
							<a href="<?php 
											echo full_url().'posts/'.anaconda_post_slug($comment->post);
											//this url need to be changed by system api comment_form_notifications()
										?>#comments">
								<?php echo (strlen($comment->text)>30?substr($comment->text, 0, 30).'...':$comment->text) ?>
							</a>
						</li>
					<?php endforeach; ?>
					</ul>
				</div>

			</div>
		</div>

		<script>
		$(function(){
			$('#recently a').click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			})
		});
		</script>
	</div>

	<div class="box">
		<h3>Meta</h3>
		<ul>
			<li><a href="<?php echo rss_url(); ?>">RSS</a></li>
			<li><a href="<?php echo base_url('admin'); ?>" title="Administer your site!">Admin</a></li>
		</ul>
	</div>
</div>