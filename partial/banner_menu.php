<header id="header" class="header header--fixed hide-from-print slide headroom--top bg-cover"" role="banner">
    <div class="container">
    
        <nav id="nav" class="nav-wrapper" role="navigation">
            <ul class="nav">
                <li class="nav__item ">
                    <a class="header__link subdued" href="https://www.github.com/WickyNilliams/headroom.js">
                        <span aria-hidden="true" class="icon icon--github"></span>
                        <span class="complimentary push--left">GitHub</span>
                    </a>
                </li>
                <li class="nav__item ">
                    <a class="header__link subdued" href="http://www.twitter.com/WickyNilliams">
                        <span aria-hidden="true" class="icon icon--twitter"></span>
                        <span class="complimentary push--left">@WickyNilliams</span>
                    </a>
                </li>
            </ul>
        </nav>
        
        <a href="http://wicky.nillia.ms/headroom.js/" class="brand header__link">
            <b class="brand__forename">Headroom</b><b class="brand__surname">.js</b>
        </a>
    </div>
</header>